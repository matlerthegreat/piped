use std::process::Stdio;

use piped_protocol::{write_output, RPCReader, Error, Result};
use tokio::process::{Command, ChildStdout, ChildStdin};
use tokio::{
    sync::mpsc::{Receiver, Sender},
    try_join,
};

use crate::message::Message;


async fn reader(tx: Sender<Message>, mut child_stdout: ChildStdout) -> Result<()> {
    let mut rpc = RPCReader::new();
    while let Some(message_data) = rpc.read_input(&mut child_stdout).await? {
        tx.send(Message::new("stdio", message_data)).await?;
    }

    Ok(())
}

async fn writter(mut rx: Receiver<Message>, mut child_stdin: ChildStdin) -> Result<()> {
    while let Some(message) = rx.recv().await {
        write_output(message.data, &mut child_stdin).await?;
    }

    Ok(())
}

pub async fn run(tx: Sender<Message>, rx: Receiver<Message>) -> Result<()> {
    let mut child = Command::new("target/debug/piped-gague")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;

    let child_stdin = child
        .stdin
        .take()
        .ok_or_else(|| Error::intput_parsing("child did not have a handle to stdin"))?;

    let child_stdout = child
        .stdout
        .take()
        .ok_or_else(|| Error::intput_parsing("child did not have a handle to stdout"))?;


    try_join!(reader(tx, child_stdout), writter(rx, child_stdin))?;

    Ok(())
}
